import 'package:flutter/material.dart';
import 'package:marvel_heroes/api/models.dart';
import 'package:transparent_image/transparent_image.dart';

class HeroDetailPage extends StatelessWidget {
  final Results hero;
  HeroDetailPage(this.hero);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Text(hero.name),
          backgroundColor: Color(0x44000000),
        ),
        body: Column(children: <Widget>[
          Hero(
            tag: hero.id,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 400,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      hero.thumbnail.path + "." + hero.thumbnail.extension),
                ),
              ),
            ),
          )
        ]));
  }
}
