import 'dart:io';

import 'package:flutter/material.dart';
import 'package:marvel_heroes/api/models.dart';
import 'package:marvel_heroes/hero_repository.dart';

class HeroListChangeNotifier extends ChangeNotifier {
  final HeroRepository heroRepository;

  HeroListChangeNotifier(this.heroRepository) {
    getResults();
  }
  int _offset = 0;
  List<Results> _results = List();

  ViewState<List<Results>> viewState;

  void getResults() async {
    if (viewState != null && viewState.status == Status.LazyLoading) {
      return;
    }

    viewState = _results.isEmpty
        ? ViewState.loading("Loading")
        : ViewState.lazyLoad(_results);
    notifyListeners();
    try {
      print("Refreshing data");
      ComicsResponse response = await heroRepository.getHeroes(_offset);
      _results.addAll(response.data.results.toList());
      viewState = ViewState.completed(_results);
      _offset += response.data.results.length;
    } catch (e) {
      viewState = ViewState.error("Oh noes! $e");
    }
    notifyListeners();
  }

  void toggleFavouriteState(int id) async {
    _results[_results.indexWhere((element) => element.id == id)].favourite =
        await heroRepository.toggleFavouriteState(id);
    viewState = ViewState.completed(_results);
    notifyListeners();
  }
}

class ViewState<T> {
  Status status;
  T data;
  String message;

  ViewState.loading(this.message) : status = Status.Loading;
  ViewState.completed(this.data) : status = Status.Success;
  ViewState.error(this.message) : status = Status.Error;
  ViewState.lazyLoad(this.data) : status = Status.LazyLoading;
}

enum Status { Loading, Success, Error, LazyLoading }
