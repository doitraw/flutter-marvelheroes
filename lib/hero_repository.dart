import 'package:marvel_heroes/cache/local_data_source.dart';

import 'api/api.dart';
import 'dart:async';
import 'api/models.dart';

class HeroRepository {
  final Api _api;
  final LocalDataSource _localDataSource;
  HeroRepository(this._api, this._localDataSource);

  Future<ComicsResponse> getHeroes(int offset) async {
    final response = await _api
        .get('v1/public/characters?limit=100&offset=' + offset.toString());
    final favourites = await _localDataSource.getFavourites();
    print("Favourites fetched");
    return ComicsResponse.fromJson(response, favourites);
  }

  Future<bool> toggleFavouriteState(int id) async {
    print("Toggling state for $id");
    return await _localDataSource.toggleFavouriteState(id);
  }
}
