import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:async';

class LocalDataSource {
  Database _database;

  Future<Database> db() async {
    if (_database == null) {
      print("Opening db!");
      _database = await openDatabase('marvel_db.db', version: 1,
          onCreate: (Database db, int version) async {
        await db.execute('''create table $_table ( 
            $_columnId integer primary key)
        ''');
      });
    }
    return _database;
  }

  Future<List<dynamic>> getFavourites() async {
    final database = await db();
    List<Map> favourites = await database.query(_table, columns: [_columnId]);
    return favourites.map((e) => e[_columnId]).toList();
  }

  Future<bool> toggleFavouriteState(int id) async {
    final database = await db();
    print("changing state for $id");
    List<Map> favourites = await database.query(_table,
        columns: [_columnId], where: '$_columnId = ?', whereArgs: [id]);
    bool isFavourite = favourites.length > 0;
    await (isFavourite ? _deleteFavourite(id) : _insertFavourite(id));
    return !isFavourite;
  }

  Future _deleteFavourite(int id) async {
    final database = await db();
    return await database
        .delete(_table, where: '$_columnId = ?', whereArgs: [id]);
  }

  Future _insertFavourite(int id) async {
    final database = await db();
    await database.insert(_table, <String, dynamic>{_columnId: id});
  }
}

final String _table = "favourites";
final String _columnId = '_id';
