import 'dart:convert';
import 'package:crypto/crypto.dart';

String generateMd5(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

class Config {
  static const String url = "https://gateway.marvel.com/";
  static const String publicKey = "2f01b0f6d2449f3bf34e11318918fdb5";
  static const String privateKey = "9807ee90365c790b9f4a754bfdac354f3093c394";
}
