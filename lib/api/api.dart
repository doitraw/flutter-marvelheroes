import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:crypto/crypto.dart';

class Api {
  final String _baseUrl = 'https://gateway.marvel.com/';
  final String _publicKey = '2f01b0f6d2449f3bf34e11318918fdb5';
  final String _privateKey = '9807ee90365c790b9f4a754bfdac354f3093c394';

  Future<dynamic> get(String url) async {
    String timestamp = DateTime.now().toString();
    final response = await http.get(_baseUrl +
        url +
        '&ts=' +
        timestamp +
        '&apikey=' +
        _publicKey +
        '&hash=' +
        generateMd5(timestamp + _privateKey + _publicKey));
    return _response(response);
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print("Great success");
        return responseJson;
      default:
        throw Exception(
            "${response.statusCode.toString()} ${response.body.toString()}");
    }
  }

  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }
}
