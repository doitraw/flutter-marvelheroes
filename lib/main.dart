import 'package:flutter/material.dart';
import 'package:marvel_heroes/api/models.dart';
import 'package:marvel_heroes/di/providers.dart';
import 'package:marvel_heroes/hero_list_change_notifier.dart';
import 'package:marvel_heroes/hero_repository.dart';
import 'package:provider/provider.dart';
import 'package:marvel_heroes/detail/hero_detail_page.dart';

void main() {
  runApp(MultiProvider(
      providers: apiProviders + cacheProviders + dataProviders,
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<HeroListChangeNotifier>(
            create: (_) =>
                HeroListChangeNotifier(Provider.of<HeroRepository>(context)),
          ),
        ],
        child: MaterialApp(
            title: 'Marvel\'s Superheroes',
            home: Scaffold(body: Center(child: Consumer<HeroListChangeNotifier>(
                builder: (context, notifier, child) {
              switch (notifier.viewState.status) {
                case Status.Loading:
                  return CircularProgressIndicator();
                  break;
                case Status.LazyLoading:
                case Status.Success:
                  return CustomScrollView(
                      controller: createController(context),
                      slivers: <Widget>[
                        SliverAppBar(
                            pinned: false,
                            expandedHeight: 250.0,
                            flexibleSpace: Stack(
                              children: <Widget>[
                                Positioned.fill(
                                    child: Image.network(
                                  "https://heronews.pl/media/2017/06/marvel-logo-1280x720.jpg",
                                  fit: BoxFit.cover,
                                ))
                              ],
                            )),
                        SliverGrid(
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2),
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                              return HeroCard(notifier.viewState.data[index]);
                            },
                            childCount: notifier.viewState.data.length,
                          ),
                        ),
                        createPagingIndicator(
                            notifier.viewState.status == Status.LazyLoading)
                      ]);
                  break;
                case Status.Error:
                default:
                  return Text(notifier.viewState.message);
                  break;
              }
            }))),
            theme: ThemeData(
              primarySwatch: Colors.blue,
            )));
  }

  ScrollController createController(BuildContext context) {
    ScrollController _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent <
          _scrollController.position.pixels + HeroCard.itemHeight * 5) {
        Provider.of<HeroListChangeNotifier>(context, listen: false)
            .getResults();
      }
    });
    return _scrollController;
  }

  SliverList createPagingIndicator(bool isPaging) {
    return SliverList(
        delegate: SliverChildListDelegate([
      isPaging
          ? Container(
              child: Center(child: CircularProgressIndicator()),
              margin: EdgeInsets.all(24.0),
            )
          : Container()
    ]));
  }
}

class HeroCard extends StatelessWidget {
  static num itemHeight = 240.0;
  final Results hero;
  HeroCard(this.hero);

  Widget heroThumb() {
    return Container(
        alignment: FractionalOffset.topLeft,
        child: Hero(
            tag: hero.id,
            child: Container(
                height: 120.0,
                width: 120.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(hero.thumbnail.path +
                          "." +
                          hero.thumbnail.extension)),
                ))));
  }

  final Widget heroCard = new Container(
    height: itemHeight,
    margin: new EdgeInsets.only(top: 60.0),
    decoration: new BoxDecoration(
      color: new Color(0xFF333366),
      shape: BoxShape.rectangle,
      borderRadius: new BorderRadius.circular(8.0),
      boxShadow: <BoxShadow>[
        new BoxShadow(
          color: Colors.black12,
          blurRadius: 10.0,
          offset: new Offset(0.0, 10.0),
        ),
      ],
    ),
  );
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => HeroDetailPage(hero)));
        },
        child: new Container(
          height: itemHeight,
          margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
          child: Stack(
            children: <Widget>[
              heroCard,
              heroThumb(),
              Align(
                child: Container(
                    child: IconButton(
                        icon: Icon(this.hero.favourite
                            ? Icons.favorite
                            : Icons.favorite_border),
                        onPressed: () => Provider.of<HeroListChangeNotifier>(
                                context,
                                listen: false)
                            .toggleFavouriteState(hero.id)),
                    margin: EdgeInsets.all(8.0)),
                alignment: Alignment.topRight,
              )
            ],
          ),
        ));
  }
}
