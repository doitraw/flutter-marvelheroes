import 'package:provider/provider.dart';
import 'package:marvel_heroes/cache/local_data_source.dart';
import 'package:provider/single_child_widget.dart';
import 'package:marvel_heroes/api/api.dart';
import 'package:marvel_heroes/hero_repository.dart';

List<SingleChildWidget> apiProviders = [Provider.value(value: Api())];

List<SingleChildWidget> cacheProviders = [
  Provider.value(value: LocalDataSource())
];

List<SingleChildWidget> dataProviders = [
  ProxyProvider2<Api, LocalDataSource, HeroRepository>(
    update: (_, api, dataSource, repository) => HeroRepository(api, dataSource),
  )
];
